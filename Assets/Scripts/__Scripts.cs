﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class __Scripts : MonoBehaviour {
    public Text text_input;

    public GameObject text_build;
    [Header("Panels")]
    public GameObject panel_build;
    public GameObject panel_cards;
    public GameObject panel_favorites;
    public GameObject panel_options;

    public Vector2 vector;
    TouchScreenKeyboard fd;


    public Text texxxxt;
    int d = 0;
    // Use this for initialization
    void Start () {
       
        AddTextBuild(8);
    }

    void AddTextBuild(int count)
    {
        for (int i = 0; i < count; i++)
        {
        GameObject str = Instantiate(text_build, panel_build.transform);
            str.transform.localPosition = new Vector3(0, 327 - 40 * i, 0);
        }  
    }

	// Update is called once per frame
	void Update () {

       // text_input.text = fd.text;
        vector = panel_build.transform.GetChild(1).localPosition;
        texxxxt.text = Accelerometer().ToString()+" @ ";
        d++;
        if (d > 100)
        {
            texxxxt.text = "";
            d = 0;
        }
            
    }

    public void TapEnterText()
    {
       
        
        
        
    }

    public void ClickPanelsBuild()
    {
        if (panel_build.activeSelf == false)
        {
            panel_build.SetActive(true);
            panel_cards.SetActive(false);
            panel_favorites.SetActive(false);
            panel_options.SetActive(false);
        }
        else
        {
            panel_build.SetActive(false);
        }
    }

    public void ClickPanelsCards()
    {
        if (panel_cards.activeSelf == false)
        {
            panel_cards.SetActive(true);
            panel_build.SetActive(false);
            panel_favorites.SetActive(false);
            panel_options.SetActive(false);
        }
        else
        {
            panel_cards.SetActive(false);
        }
    }

    public void ClickPanelsFavorites()
    {
        if (panel_favorites.activeSelf == false)
        {
            panel_favorites.SetActive(true);
            panel_cards.SetActive(false);
            panel_build.SetActive(false);
            panel_options.SetActive(false);
        }
        else
        {
            panel_favorites.SetActive(false);
        }
    }

    public void ClickPanelsOptions()
    {
        if (panel_options.activeSelf == false)
        {
            panel_options.SetActive(true);
            panel_cards.SetActive(false);
            panel_favorites.SetActive(false);
            panel_build.SetActive(false);
        }
        else
        {
            panel_options.SetActive(false);
        }
    }
    float speed = 10f;
    public Vector3 Accelerometer()
    {
        Vector3 dir = Vector3.zero;

        // we assume that the device is held parallel to the ground
        // and the Home button is in the right hand

        // remap the device acceleration axis to game coordinates:
        // 1) XY plane of the device is mapped onto XZ plane
        // 2) rotated 90 degrees around Y axis
        dir.x = Input.acceleration.x;
        dir.z = Input.acceleration.z;
        dir.y = Input.acceleration.y;
        return dir;
        // clamp acceleration vector to the unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        // Make it move 10 meters per second instead of 10 meters per frame...
        dir *= Time.deltaTime;

        // Move object
        transform.Translate(dir * speed);
        return transform.position;
    }
}
