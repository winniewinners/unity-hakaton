﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class NewBehaviourScript : MonoBehaviour {

    private float originalLatitude;
    private float originalLongitude;
    private float currentLongitude;
    private float currentLatitude;

    private GameObject distanceTextObject;
    private double distance;

    private bool setOriginalValues = true;

    private Vector3 targetPosition;
    private Vector3 originalPosition;

    private float speed = .1f;


    public Text gps1;
    public Text gps2;
    public Text pos1;
    public Text pos2;
    public Text error;
    public Text dist;

    public GameObject bulavka;

    Vector2 backPosition;
    Vector3 backRotation;
    IEnumerator GetCoordinates()
    {
        //while true so this function keeps running once started.
        while (true)
        {
            error.text = ("Coruntina start");
            // check if user has location service enabled
            if (!Input.location.isEnabledByUser)
                yield break;

            // Start service before querying location
            Input.location.Start(1f, .1f);

            // Wait until service initializes
            int maxWait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing /*&& maxWait > 0*/)
            {
                yield return new WaitForSeconds(0.1f);
                maxWait--;
            }

            // Service didn't initialize in 20 seconds
            if (maxWait < 1)
            {
                error.text=("Timed out");
                yield break;
            }

            // Connection has failed
            if (Input.location.status == LocationServiceStatus.Failed)
            {
                error.text = ("Unable to determine device location");
                yield break;
            }
            else
            {
                // Access granted and location value could be retrieved
                error.text = ("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);

                //if original value has not yet been set save coordinates of player on app start
                if (setOriginalValues)
                {
                    originalLatitude = Input.location.lastData.latitude;
                    originalLongitude = Input.location.lastData.longitude;
                    setOriginalValues = false;
                }

                //overwrite current lat and lon everytime
                currentLatitude = Input.location.lastData.latitude;
                currentLongitude = Input.location.lastData.longitude;
                gps1.text = "GPS: " + (currentLatitude + "  " + currentLongitude);
                //gps2.text="GPS: "+(currentLatitude+"  "+ currentLongitude);
                //calculate the distance between where the player was when the app started and where they are now.
                Calc(backPosition.x, backPosition.y, currentLatitude, currentLongitude);

            }
            Input.location.Stop();
            error.text = ("Coruntina end");
        }
    }

    //calculates distance between two sets of coordinates, taking into account the curvature of the earth.
    public void Calc(float lat1, float lon1, float lat2, float lon2)
    {

        var R = 6378.137; // Radius of earth in KM
        var dLat = lat2 * Mathf.PI / 180 - lat1 * Mathf.PI / 180;
        var dLon = lon2 * Mathf.PI / 180 - lon1 * Mathf.PI / 180;
        float a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) +
          Mathf.Cos(lat1 * Mathf.PI / 180) * Mathf.Cos(lat2 * Mathf.PI / 180) *
          Mathf.Sin(dLon / 2) * Mathf.Sin(dLon / 2);
        var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        distance = R * c;
        distance = distance * 1000f; // meters
                                     //set the distance text on the canvas
        //distanceTextObject.GetComponent<Text>().text = "Distance: " + distance;
        //convert distance from double to float
        float distanceFloat = (float)distance;
        if(backPosition!=new Vector2(0,0))
            this.transform.position += new Vector3(lat2 - lat1,0, lon2 - lon1)*1110000;
        backPosition = new Vector2(lat2, lon2);
        pos2.text = transform.position.x + "  " + transform.position.z;
        //set the target position of the ufo, this is where we lerp to in the update function
        //targetPosition = originalPosition - new Vector3(0, 0, distanceFloat * 12);
        //distance was multiplied by 12 so I didn't have to walk that far to get the UFO to show up closer

    }
    public Transform cameraTransform;
    void Start()
    {
        transform.position = new Vector3(Input.location.lastData.latitude, 0, Input.location.lastData.longitude) * 1110000;
        bulavka.transform.position = new Vector3(54.17302f, 0, 37.59267f) * 1110000;
        //get distance text reference
        distanceTextObject = GameObject.FindGameObjectWithTag("distanceText");

        error.text = ("Enter coruntina");
        backPosition =new Vector2( Input.location.lastData.latitude,Input.location.lastData.longitude);
        //start GetCoordinate() function 
        StartCoroutine("GetCoordinates");
        //initialize target and original position
        //targetPosition = transform.position;
        //originalPosition = transform.position;
        ///
        //Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //cameraTransform = Camera.main.transform;



        
    }

    void Update()
    {
        //pos1.text = Input.acceleration.x + "  " + Input.acceleration.y + "  " + Input.acceleration.z;
        pos1.text = bulavka.transform.position.x + "  " + bulavka.transform.position.z;
        pos2.text = transform.position.x + "  " + transform.position.y;
        dist.text = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(bulavka.transform.position.x, bulavka.transform.position.z)).ToString();
        //this.transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y- 5, transform.rotation.z, 0);


        //linearly interpolate from current position to target position
        //gps1.text = transform.rotation.x + "  " + transform.rotation.y + "  " + transform.position.z;
       // gps2.text = transform.position.x + "  " + transform.position.y + "  " + transform.position.z;
        ///transform.position = Vector3.Lerp(transform.position, targetPosition, speed);
        //rotate by 1 degree about the y axis every frame
        ///transform.eulerAngles += new Vector3(0, 1f, 0);
        ///
        //float accX, accY;
        //if (Mathf.Abs(Input.acceleration.x) > 0.1f) { accX = Input.acceleration.x * 2; }
        //if (Mathf.Abs(Input.acceleration.y) > 0.1f) { accY = Input.acceleration.y * 2; }

        //Vector3 screenPlayer = Camera.main.WorldToScreenPoint(player.position);
        //Vector3 accScreen = new Vector3(screenPlayer.x + accX, screenPlayer.y + accY, 0);
        //transform.position = Camera.main.ScreenToWorldPoint(accScreen);

        ////Vector3 diff = transform.position - player.position;
        //diff.Normalize();

        //float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        //   player.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);




        //transform.rotation = Input.gyro.attitude;

    }


    private Vector3 defaultRotate = new Vector3(0, 180, 0);


    public float maxAngle = 3f;

    

    private Vector3 accelerator;



    private void Awake()
    {
       // Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private Vector3 stepRotete = new Vector3(10, 10, 0);

    private void FixedUpdate()
    {
        
        if (Input.acceleration.x < -0.1f)
            transform.Rotate(Vector3.down);
        if (Input.acceleration.x > 0.1f)
            transform.Rotate(Vector3.up);
        //accelerator = Input.acceleration;

        //var currentRotate = new Vector3(cameraTransform.rotation.eulerAngles.x,
        //                                cameraTransform.rotation.eulerAngles.y,
        //                                cameraTransform.rotation.eulerAngles.z);

        //var newRotate = new Vector3(
        //    CorrectAngleX(currentRotate.x + accelerator.y * stepRotete.x),
        //    CorrectAngleY(currentRotate.y - accelerator.x * stepRotete.y),
        //    0);

        //cameraTransform.rotation = Quaternion.Lerp(cameraTransform.rotation, Quaternion.Euler(newRotate),
        //                                            Time.time * speed);

    }

    private float CorrectAngleX(float angle)
    {
        float res = angle;
        if (angle > 180 && angle < 360 - maxAngle)
        {
            res = 360 - maxAngle;
        }
        if (angle < 180 && angle > maxAngle)
        {
            res = maxAngle;
        }

        return res;
    }

    private float CorrectAngleY(float angle)
    {
        float res = angle;

        res = Mathf.Clamp(angle, 180 - maxAngle, 180 + maxAngle);

        return res;
    }
}
