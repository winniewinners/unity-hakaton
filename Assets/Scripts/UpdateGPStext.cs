﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateGPStext : MonoBehaviour
{
    public Text coords;
    public Text text;
    public int i = 0;

    public double latitude;
    public double longitude;
    private void Update()
    {
        
        i++;
        StartLocationService();
        if (i > 10000)
            i = 0;

        //coords.text = "Lat: " + GPS.Instance.latitude + "\nLog: " + GPS.Instance.longitude + "\nCity: " + GPS.Instance.city;

    }

    void StartLocationService()
    {
        if (!Input.location.isEnabledByUser)
        {
            text.text = ("Пользователь не подключен к GPS");
            return;//yield break;
        }

        Input.location.Start();
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            //yield return new WaitForSeconds(1);
            maxWait--;

            if (maxWait <= 0)
            {
                text.text = ("Время вышло");
                return;//yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                text.text = ("Невозможно определить положение");
                return;//yield break;
            }
            i++;
            text.text = i.ToString();
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;
            coords.text = "Lat: " + GPS.Instance.latitude + "\nLog: " + GPS.Instance.longitude;

            return;//yield break;
        }
    }

}
