﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using System;
using System.Net;
using System.Linq;
using System.Xml.Linq; 

public class GPS : MonoBehaviour
{
    private static string baseUri = "https://maps.googleapis.com/maps/api/geocode/xml?latlng={0},{1}&key=AIzaSyCJWRPvw6Bg66CC_QRE8ymC6Ey_9uWkNew";

    public static GPS Instance { get; set; }

    public Text text;
    public Text text_sities;
    public Text text2;

    public float latitude;
    public float longitude;

    int i = 0;

    public string city="@@";

    List<int> aa = new List<int>() { 2, 3, 4, 5, 6, 7, 89, 9 };
	void Start ()
    {
        text2.text = aa.ToArray().Sum().ToString();
        Instance = this;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
        latitude = 54.16698f;
        longitude = 37.58561f;
        RetrieveFormatedAddress();
        text_sities.text = city;
        
    }

    IEnumerator StartLocationService()
    {
        if (!Input.location.isEnabledByUser)
        {
            text.text = ("Пользователь не подключен к GPS");
            yield break;
        }

        Input.location.Start();
        int maxWait = 20;
        while(Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;

            if (maxWait <= 0)
            {
                text.text=("Время вышло");
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {
                text.text = ("Невозможно определить положение");
                yield break;
            }
            i++;
            text.text = i.ToString();
            latitude = Input.location.lastData.latitude;
            longitude = Input.location.lastData.longitude;

            yield break;
        }
    }

    void RetrieveFormatedAddress()
    {
        string requestUri = string.Format(baseUri, latitude.ToString(), longitude.ToString());

        using (WebClient wc = new WebClient())
        {
            wc.DownloadStringCompleted += new DownloadStringCompletedEventHandler(wc_DownloadStringCompleted);
            wc.DownloadStringAsync(new Uri(requestUri));
        }
    }

    void wc_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
    {
        var xmlElm = XElement.Parse(e.Result);

        var status = (from elm in xmlElm.Descendants()
                      where elm.Name == "status"
                      select elm).FirstOrDefault();
        if (status.Value.ToLower() == "ok")
        {
            var res = (from elm in xmlElm.Descendants()
                       where elm.Name == "formatted_address"
                       select elm).FirstOrDefault();
            city = res.Value;
        }
        else
        {
            city = "Адрес не найден";
            return;
        }
    }
}
